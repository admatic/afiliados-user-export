FROM node:8.11.3-alpine

# FROM registry.b2w.io/afiliados/nodejs8-onbuild:1.0

# change
WORKDIR /var/current

# Bundle app source
COPY . /var/current

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN npm install --production

ENV NODE_ENV development
ENV LOG_LEVEL info
ENV UV_THREADPOOL_SIZE 16
