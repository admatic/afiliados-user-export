const moment = require('moment-timezone')

const PORT = process.env.AFL_PORT || 8080

const TIMEZONE = process.env.AFL_TIMEZONE || 'America/Sao_Paulo'

moment.tz.setDefault(TIMEZONE)

module.exports = {
  PORT,
  TIMEZONE,
}
