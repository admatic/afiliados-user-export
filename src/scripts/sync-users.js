const moment = require('moment')

/* 
Databases and models
*/
const LegacyDb = require('../initializers/legacy')();
const ProductionDb = require('../initializers/sequelize')();

async function main() {

  try {
    const users = await LegacyDb.query('select * from admin_usuarios', { type: LegacyDb.QueryTypes.SELECT })

      try {
        console.log('Começou iteração')
        for (let i = 0; i <= users.length; i++) {
          console.log('index: ', i)

          const user = users[i]

          await insertUsers(user, i)

        }
      
      } catch (err) {

        console.error(err) 

      }

    process.exit(0)
    
  } catch(err) {

    console.error(err)

    process.exit(1)
  }

}

async function insertUsers(user, i){
  console.log('Salvando ', i)
  

  return ProductionDb.query( `insert into admin_usuarios
  (user_id,senha,nova_senha,nome,nascimento,email,cpf_ANTIGO, cpf
  ,rg,expeditor,banco,n_banco,agencia,conta,url,nome_site,classificacao
  ,ddd_fixo,telefone_fixo,ddd_celular,telefone_celular,pais,cep,estado,cidade
  ,endereco,numero,complemento,bairro,pageviews,descricao,nome_responsavel,status
  ,num_usuarios,criacao,tipo,email_marketing,tamanho_base,data_atualizacao,ip,status_novo
  ,status_motivo,categoria,alerta_email,razao,iestadual,perfil,receber_email,extra,funcionario
  ,hash,status_documentacao,mei,termo)
  values
  (?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?,?,?
  ,?,?,?,?,?,?)
  on duplicate key update
  cpf=?,banco=?,n_banco=?,agencia=?,conta=?,url=?,nome_site=?,ddd_fixo=?
  ,telefone_fixo=?,ddd_celular=?,telefone_celular=?,pais=?,cep=?,estado=?,cidade=?
  ,endereco=?,numero=?,complemento=?,bairro=?,descricao=?,nome_responsavel=?,tipo=?
  ,data_atualizacao=?,status_novo=?,iestadual=?,status_documentacao=?,mei=?`, { replacements: [
    user.user_id,user.senha,user.nova_senha,user.nome,user.nascimento,user.email,user.cpf_ANTIGO,user.cpf
    ,user.rg,user.expeditor,user.banco,user.n_banco,user.agencia,user.conta,user.url,user.nome_site,user.classificacao
    ,user.ddd_fixo,user.telefone_fixo,user.ddd_celular,user.telefone_celular,user.pais,user.cep,user.estado,user.cidade
    ,user.endereco,user.numero,user.complemento,user.bairro,user.pageviews,user.descricao,user.nome_responsavel,user.status
    ,user.num_usuarios,user.criacao,user.tipo,user.email_marketing,user.tamanho_base,user.data_atualizacao,user.ip,user.status_novo
    ,user.status_motivo,user.categoria,user.alerta_email,user.razao,user.iestadual,user.perfil,user.receber_email,user.extra,user.funcionario
    ,user.hash,user.status_documentacao,user.mei,user.termo,


    user.cpf,user.banco,user.n_banco,user.agencia,user.conta,user.url,user.nome_site,user.ddd_fixo
    ,user.telefone_fixo,user.ddd_celular,user.telefone_celular,user.pais,user.cep,user.estado,user.cidade
    ,user.endereco,user.numero,user.complemento,user.bairro,user.descricao,user.nome_responsavel,user.tipo
    ,user.data_atualizacao,user.status_novo,user.iestadual,user.status_documentacao,user.mei
  ], type: ProductionDb.QueryTypes.INSERT
  })

}

Promise.resolve(main())