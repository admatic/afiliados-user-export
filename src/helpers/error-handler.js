const { logger } = require('afiliados-logger')

// Wrap route functions repassing exceptions to error handling
function wrapAsync(fn) {
  return (req, res, next) => {
    fn(req, res, next).catch(next)
  }
}

function errorHandler(err, req, res, next) {
  logger.error(err)
  res.status(400).json({
    message: err.message,
  })
  next()
}

module.exports = {
  wrapAsync,
  errorHandler,
}
