const moment = require('moment-timezone')
const Sequelize = require('sequelize')
const _ = require('lodash')

const FLOAT_TYPES = [
  Sequelize.FLOAT,
  Sequelize.DOUBLE,
  Sequelize.DECIMAL,
  Sequelize.REAL,
]

const DATE_TYPES = [
  Sequelize.DATE,
  Sequelize.DATEONLY,
]

function forceDecimalsAsFloat(attributes) {
  const attrs = Object.assign({}, attributes)

  Object.keys(attrs).forEach((key) => {
    const type = attrs[key].type || attrs[key]
    if (!isInstance(type, FLOAT_TYPES)) {
      return
    }

    if (!attrs[key].type) {
      attrs[key] = { type }
    }

    if (!attrs[key].get) {
      attrs[key].get = getFloat(key)
    }
  })

  return attrs
}

function forceFormatDate(attributes) {
  const attrs = Object.assign({}, attributes)

  Object.keys(attrs).forEach((key) => {
    const type = attrs[key].type || attrs[key]

    if (!isInstance(type, DATE_TYPES)) {
      return
    }

    if (!attrs[key].type) {
      attrs[key] = { type }
    }

    if (attrs[key].get) {
      return
    }

    if (isInstance(type, Sequelize.DATE)) {
      attrs[key].get = getDateTime(key)
    }

    if (isInstance(type, Sequelize.DATEONLY)) {
      attrs[key].get = getDateOnly(key)
    }
  })

  return attrs
}

function getFloat(key) {
  return function get() {
    // eslint-disable-next-line no-underscore-dangle
    const { options } = this._modelOptions.sequelize.models[this._modelOptions.name.singular]
      .tableAttributes[key]
      .type

    const scale = options.scale || options.decimals

    if (this.getDataValue(key) === undefined) return undefined

    const value = parseFloat(this.getDataValue(key))
    return scale ? _.round(value, scale) : value
  }
}

function getDateTime(key) {
  return function get() {
    if (this.getDataValue(key) === undefined) return undefined
    return moment(this.getDataValue(key)).format('YYYY-MM-DD HH:mm:ss')
  }
}

function getDateOnly(key) {
  return function get() {
    if (this.getDataValue(key) === undefined) return undefined
    return moment(this.getDataValue(key)).format('YYYY-MM-DD')
  }
}

function isInstance(obj, types) {
  const typeslist = Array.isArray(types) ? types : [types]

  return typeslist.some((type) => {
    if (obj === type) {
      return true
    }

    try {
      return obj instanceof type
    } catch (err) {
      return false
    }
  })
}

module.exports = {
  isInstance,
  forceDecimalsAsFloat,
  forceFormatDate,
  getDateOnly,
  getDateTime,
  getFloat,
}
