const Sequelize = require('sequelize')
const envLoader = require('env-o-loader')

const {
  primary: primarySqlUrl,
  config,
} = envLoader('../config/legacy.yaml')

module.exports = () => {
  const sequelize = new Sequelize(primarySqlUrl, config)
  return sequelize
}
