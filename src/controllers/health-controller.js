const sequelize = require('../models').sql.loadModel()

async function healthCheck(req, res) {
  sequelize.authenticate()
    .then(() => res.json({ db: true }))
    .catch(() => res.status(500).json({ db: false }))
}

module.exports = {
  healthCheck,
}
