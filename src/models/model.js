const { forceDecimalsAsFloat, forceFormatDate } = require('../helpers/helper')

module.exports = function model(db, tableName, attributes, options = {}) {
  let attrs = forceDecimalsAsFloat(attributes)
  attrs = forceFormatDate(attrs)

  return db.define(tableName, attrs, Object.assign({
    timestamps: false,
    freezeTableName: true,
  }, options))
}
