const Sequelize = require('sequelize');
const model = require('../model');

const BalanceHistoryInsertionType = require('../').sql.loadModel('balance-history-insertion-type')

module.exports = (sequelize) => model(sequelize, 'balance_history', {
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  amount: {
    type: Sequelize.DECIMAL(10, 2),
    allowNull: false,
  },
  description: Sequelize.STRING,
  insertion_type_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: BalanceHistoryInsertionType,
      key: 'id',
    },
  },
  created_at: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
  metadata: Sequelize.TEXT,
});