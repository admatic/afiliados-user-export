const Sequelize = require('sequelize');
const model = require('../model');

module.exports = (sequelize) => model(sequelize, 'balance_history_insertion_type', {
  description: {
    type: Sequelize.STRING(255),
    allowNull: false,
  },
  created_at: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
});

module.exports.Type = {
  BALANCE_UPDATER: 1,
  MANUAL_INPUT: 2,
  INVOICE_GENERATOR: 3,
};