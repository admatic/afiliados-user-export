const Sequelize = require('sequelize')
const decamelize = require('decamelize')

const sequelize = require('../../initializers/sequelize')()

const cache = {}

exports.loadModel = (name) => {
  if (!name) {
    return sequelize
  }

  const decamelizedName = decamelize(name, '-')

  if (!cache[decamelizedName]) {
    /* eslint-disable global-require */
    const model = require(`${__dirname}/${decamelizedName}.js`)(sequelize, Sequelize.DataTypes)
    /* eslint-enable global-require */
    cache[decamelizedName] = model

    if (model.associate) {
      model.associate(cache)
    }
  }

  return cache[decamelizedName]
}
