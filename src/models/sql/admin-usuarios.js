const Sequelize = require('sequelize');
const model = require('../model');

module.exports = (sequelize) => model(sequelize,  'admin_usuarios', {
  user_id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
  },
  senha: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  nova_senha: {
    type: Sequelize.STRING(255),
    allowNull: false,
  },
  nome: {
    type: Sequelize.STRING(200),
    allowNull: false,
  },
  nascimento: {
    type: Sequelize.DATEONLY,
  },
  email: {
    type: Sequelize.STRING(100),
    allowNull: false,
    unique: true,
  },
  cpf: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  banco: {
    type: Sequelize.STRING(80),
    allowNull: false,
    defaultValue: '',
  },
  n_banco: {
    type: Sequelize.STRING(10),
    allowNull: false,
    defaultValue: '',
  },
  agencia: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: '',
  },
  conta: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: '',
  },
  ddd_fixo: {
    type: Sequelize.INTEGER(6),
  },
  telefone_fixo: {
    type: Sequelize.STRING(15),
    allowNull: false,
    defaultValue: '',
  },
  cep: {
    type: Sequelize.STRING(12),
    allowNull: false,
    defaultValue: '',
  },
  estado: {
    type: Sequelize.STRING(30),
    allowNull: false,
    defaultValue: '',
  },
  cidade: {
    type: Sequelize.STRING(30),
    allowNull: false,
    defaultValue: '',
  },
  endereco: {
    type: Sequelize.STRING(100),
    allowNull: false,
    defaultValue: '',
  },
  numero: {
    type: Sequelize.STRING(10),
    allowNull: false,
    defaultValue: '',
  },
  complemento: {
    type: Sequelize.STRING(40),
    allowNull: false,
    defaultValue: '',
  },
  bairro: {
    type: Sequelize.STRING(30),
    allowNull: false,
    defaultValue: '',
  },
  criacao: {
    type: Sequelize.DATE,
    defaultValue: () => new Date(),
  },
  tipo: {
    type: Sequelize.STRING(2),
  },
  email_marketing: {
    type: Sequelize.INTEGER(2),
  },
  data_atualizacao: {
    type: Sequelize.DATE,
    defaultValue: () => new Date(),
  },
  ip: {
    type: Sequelize.STRING(15),
  },
  status_novo: {
    type: Sequelize.INTEGER(4),
    defaultValue: 2,
  },
  categoria: {
    type: Sequelize.INTEGER(9),
    defaultValue: 0,
  },
  razao: {
    type: Sequelize.STRING(200),
  },
  iestadual: {
    type: Sequelize.STRING(50),
  },
  receber_email: {
    type: Sequelize.INTEGER(4),
    defaultValue: 1,
  },
  status_documentacao: {
    type: Sequelize.INTEGER(1),
  },
  termo: {
    type: Sequelize.INTEGER(4),
    allowNull: false,
    defaultValue: 1,
  },
  mei: {
    type: Sequelize.BOOLEAN,
  },
});
