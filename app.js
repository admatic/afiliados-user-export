const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { wrapAsync, errorHandler } = require('./src/helpers/error-handler')

const HealthController = require('./src/controllers/health-controller')

const app = express()

app.use(cors())
app.use(bodyParser.json())


app.get('/api/v1/health', HealthController.healthCheck)

// Default express error handling
app.use(errorHandler)

module.exports = app